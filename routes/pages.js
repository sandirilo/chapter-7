const router = require("express").Router()
const page = require("../controllers/pageController");

router.get("/", page.home);

module.exports =  router

