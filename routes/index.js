const express = require("express");
const router = express.Router();
const pageRouter = require("./pages")

/* GET home page. */
router.use("/", pageRouter);

module.exports = router;
